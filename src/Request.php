<?php

namespace LuciferP\Http;

/**
 * Class Request
 * @package LuciferP
 * @author Luficer.p <81434146@qq.com>
 */
class Request implements \IteratorAggregate, \ArrayAccess
{
    /**
     * @var array
     */
    private $get;
    /**
     * @var array
     */
    private $post;
    /**
     * @var array
     */
    private $cookies;
    /**
     * @var array
     */
    private $env;
    /**
     * @var array
     */
    private $server = [

    ];
    /**
     * @var array
     */
    private $requests = [];
    /**
     * @var string
     */
    private $homeUrl;
    /**
     * @var string
     */
    private $url;
    /**
     * @var
     */
    private $uri;
    /**
     * @var
     */
    private $method;

    /**
     * Request constructor.
     */
    function __construct()
    {

        $this->get = $this->filter($_GET);
        $this->post = $this->filter($_POST, INPUT_POST);
        $this->cookies = $this->filter($_COOKIE, INPUT_COOKIE);
        $this->env = $this->filter($_ENV, INPUT_ENV);
        $this->server = $this->getServer();
        $this->homeUrl = $this->getHomeUrl();
        $this->url = $this->getUrl();
        $this->uri = $this->server['REQUEST_URI'];
        $this->method = $this->server['REQUEST_METHOD'];

    }

    /**
     * @param $params
     * @param int $type
     * @return array
     */
    private function filter($params, $type = INPUT_GET)
    {
        $ret = [];
        foreach ($params as $key => $param) {
            $ret[$key] = filter_input($type, $key, FILTER_DEFAULT);
        }
        return $ret;
    }

    private function getServer()
    {
        $def = [
            'REQUEST_URI' => '/',
            'REQUEST_METHOD' => 'GET',
            'HTTP_HOST' => 'localhost',
        ];
        $server = $_SERVER;//$this->filter($_SERVER,INPUT_SERVER);
        if (!isset($server['REQUEST_URI'])) {
            $server = array_merge($server, $def);
        }

        return $server;
    }

    /**
     * @return string
     */
    private function getHomeUrl()
    {
        $prfix = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
//        $dir = preg_replace("#index\.php#", "", $this->server['SCRIPT_NAME']);
        $url = $prfix . $this->server['HTTP_HOST'];

        return $url;

    }

    /**
     * @return string
     */
    private function getUrl()
    {
        return $this->getHomeUrl() . $this->server['REQUEST_URI'];
    }

    /**
     * @param $get
     */
    public function setGetParams($get)
    {
        $this->get = $get;
        $this->requests['get'] = $get;

    }


    function getIterator()
    {
        return new \ArrayIterator($this);
    }


    function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    function offsetGet($offset)
    {
        return $this->$offset;
    }

    function offsetSet($offset, $value)
    {
//        $this->$offset = $value;
        throw new \Exception("can't set this value:{$value}");
    }

    function offsetUnset($offset)
    {
        throw new \Exception("can't unset this offset:{$offset}");
    }

}