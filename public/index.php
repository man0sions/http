<?php
/**
 * Created by PhpStorm.
 * @author Luficer.p <81434146@qq.com>
 * Date: 16/9/22
 * Time: 下午3:29
 */
define("BASE_PATH", __DIR__);
require BASE_PATH . '/../vendor/autoload.php';

$request = new \LuciferP\Http\Request();
//var_dump($request['server']); //等价于 $_SERVER
//var_dump($request['get']);    //等价于 $_GET 并进行filter_input
//var_dump($request['post']);   //等价于 $_POST 并进行filter_input
//var_dump($request['cookie']); //等价于 $_COOKIE 并进行filter_input
//var_dump($request['env']);    //等价于 $_ENV 并进行filter_input
//var_dump($request['homeUrl']);//等价于 http/https+$_SERVER['HTTP_HOST']
//var_dump($request['method']); //等价于 $_SERVER['REQUEST_METHOD']
//var_dump($request['uri']);    //等价于 $_SERVER['REQUEST_URI']

$response = new \LuciferP\Http\Response();
/**
 * $response->status() 设置返回码[默认200] 200,404,500 ...
 * $response->type()   设置返回类型[默认 text/html] text/json...
 * $response->json()   在页面输出json
 * $response->jsonp()  在页面输出jsonp
 * $response->render() 把数据渲染到指定的页面
 */
//  $response->status(200)->send(json_encode($req));
//  $response->type('text/json')->send(json_encode($req));
//  $response->json(['hello'=>'world']);
//  $response->jsonp(['hello'=>'world']);
//  $response->redirect("http://baidu.com");
$response->status(200)->type('text/html')->render(BASE_PATH . "/../views/view.php", ['name' => 'zhangsan', 'age' => 20]);


